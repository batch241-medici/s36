// Create the Schema, model and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: String
});

// tasks
module.exports = mongoose.model("Task", taskSchema);

// "module.exports" is way